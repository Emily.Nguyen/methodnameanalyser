package com.github.emilymnl.methodnameanalyser.NLP;

import java.util.Properties;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.sentiment.SentimentCoreAnnotations;
import edu.stanford.nlp.util.CoreMap;

public class SA {
    public static StanfordCoreNLP pipeline;

    public static void init() {
        Properties props = new Properties();
        props.setProperty("annotators", "tokenize, ssplit, parse, sentiment");
        pipeline = new StanfordCoreNLP(props);
    }

    public static String findSentiment(String input) {
        String sentimentType = "NULL";
        if (input != null && input.length() > 0) {
            Annotation annotation = pipeline.process(input);
            for (CoreMap sentence : annotation.get(CoreAnnotations.SentencesAnnotation.class)) {
                sentimentType = sentence.get(SentimentCoreAnnotations.SentimentClass.class);
            }
        }
        return sentimentType;
    }

    public static String getSentiment(String sentence) {
        init();
    
        String sentiment = SA.findSentiment(sentence);
        if (sentiment.equalsIgnoreCase("Neutral")){
            return "neutral";
        } else if (sentiment.equalsIgnoreCase("Negative") || sentiment.equalsIgnoreCase("Very Negative")){
            return "negative";
        } else if (sentiment.equalsIgnoreCase("Positive") || sentiment.equalsIgnoreCase("Very Positive")){
            return "positive";
        } else {
            return "unspecified";
        }
    }

    public static Boolean checkSentiments(String userSentiment, String methodName) {
        String sentiment = getSentiment(methodName);
        return sentiment.equals(userSentiment);
    }

}


