package com.github.emilymnl.methodnameanalyser.NLP;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.regex.*;

import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSSample;
import opennlp.tools.postag.POSTaggerME;

public class POS {
    public static boolean checkRegexToName(String regex, String name) throws IOException {
        // removes dot and space from regex
        String cleanRegex = regex.replaceAll("\\.", "").replaceAll("\\s+", "");
        Pattern pattern = Pattern.compile(cleanRegex, Pattern.CASE_INSENSITIVE);
        String POSOfName = getPOSOfName(name);
        String POSOfNameWithString = getPOSOfNameWithString(name, regex);
        // check pos to POS and string to string
        return pattern.matcher(POSOfName).matches() || pattern.matcher(POSOfNameWithString).matches(); 
    }
    
    // use POS library to get POS tags/sentence of sentence
    public static String[] getPOS(String methodName, boolean getTags) throws IOException {
        InputStream inputStream = new FileInputStream("./src/main/java/com/github/emilymnl/methodnameanalyser/NLP/en-pos-maxent.bin"); 
        POSModel model = new POSModel(inputStream);
        // Instantiating POSTaggerME class 
        POSTaggerME tagger = new POSTaggerME(model);

        String[] separatedBeforeCapital = separateAtCapitalLetter(methodName);
        // Generating tags 
        String[] tags = tagger.tag(separatedBeforeCapital); 
        // Instantiating the POSSample class 
        POSSample sample = new POSSample(separatedBeforeCapital, tags);

        ArrayList<String> listOfWordAndSentiment = new ArrayList<String>();
        ArrayList<String> listOfTagAndSentiment = new ArrayList<String>();
        
        for (int wordIndex = 0; wordIndex < separatedBeforeCapital.length; wordIndex++) {
            String sentiment = SA.getSentiment(sample.getSentence()[wordIndex]);
            listOfWordAndSentiment.add(sample.getSentence()[wordIndex]+"@"+sentiment);
            listOfTagAndSentiment.add(sample.getTags()[wordIndex]+"@"+sentiment);
        }
        if (getTags) {
            // make arraylist to string array
            return listOfTagAndSentiment.toArray(new String[0]);
        } else { 
            return listOfWordAndSentiment.toArray(new String[0]);
        }
    }

    // Tokenizing the sentence with regex - separates at cap letter, then lowercase them all, and trims leading and trailing whitespace
    public static String[] separateAtCapitalLetter(String methodName) {
        return methodName.replaceAll("((?<=[a-z])[A-Z]|[A-Z](?=[a-z]))", " $1").toLowerCase().trim().split("\\s+");
    }

    public static String getPOSOfNameWithString(String name, String regex) throws IOException {
        String[] nameTag = getPOS(name, true);
        String[] nameWord = getPOS(name, false);

        String regexWordOnly = regex.replaceAll("[(.*+?|)]", " ");
        String[] regexArray = regexWordOnly.trim().split("\\s+");

        // loop through regex, find the string and replace instead of POS tag
        String[] POSOfName = nameTag;
        for (int i = 0; i < regexArray.length; i++) {
            for (int j = 0; j < nameWord.length; j++) {
                if (regexArray[i].equals(nameWord[j])) {
                    POSOfName[j] = regexArray[i];
                }
            }
        }
        // return POS with or without the string
        return String.join("", POSOfName);
    }

    public static String getPOSOfName(String name) throws IOException {
        // return POS of name
        return String.join("", getPOS(name, true));
    }
    
}

