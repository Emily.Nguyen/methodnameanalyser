package com.github.emilymnl.methodnameanalyser;

import java.io.Console;
import java.io.IOException;
import java.util.LinkedHashSet;
import java.util.Set;

import net.sf.extjwnl.*;

public class App {
    public static void main(String[] args) throws IOException, JWNLException, CloneNotSupportedException {
        runIOConsole();
    }

    public static void runIOConsole() {
        String inputaPathToXML = "";
        String fileFromGitHub = "";
        String inputRawFileLink = "";
        
        Set<String> checkList = new LinkedHashSet<>();
        checkList.add("P : to pass the XML file with requirements");
        checkList.add("I : to import raw Java source file from GitHub");
        checkList.add("Q : to quit");

        Console c = System.console();
        if (c == null) {
            System.err.println("No console.");
            System.exit(1);
        }
        try {
            boolean noQuit = true;
            while (noQuit) {
                if (checkList.size()-1 != 0) {
                    System.out.println("--------------------------------------\n"
                                + "| Please perform the top " + (checkList.size()-1) + " action(s) |");
                }
                System.out.println("--------------------------------------");
                for (String checkElement : checkList) {
                    System.out.println("| " + checkElement + "");
                }

                String input = c.readLine("| Your choice: ");

                if (input.toLowerCase().equals("q")) {
                    System.out.println("\n--- THANK YOU ---\n");
                    noQuit = false;
                } else if (input.toLowerCase().equals("p")) {
                    checkList.remove(new String("P : to pass the XML file with requirements"));
                    inputaPathToXML = c.readLine("| Path to requirements: ");
                    System.out.println();
                    if (checkList.size() == 1) { 
                        printAndAnalyse(inputRawFileLink, fileFromGitHub, inputaPathToXML);
                        break;
                    }
                } else if (input.toLowerCase().equals("i")) {
                    checkList.remove(new String("I : to import raw Java source file from GitHub"));
                    inputRawFileLink = c.readLine("| Raw file link from GitHub: ");
                    System.out.println();
                    fileFromGitHub = downloadFileFromGitHub(inputRawFileLink);
                    if (checkList.size() == 1) {
                        printAndAnalyse(inputRawFileLink, fileFromGitHub, inputaPathToXML);
                        break;
                    }
                } else {
                    System.out.println("\n'" + input + "' not accepted, only P, I or Q. Please try again.\n");
                }
            }
        } catch (Exception e) {
            System.err.print("Exception message: " + e.getMessage()+ "\n");
        }
    }

    public static String downloadFileFromGitHub(String rawLinkToFile) throws IOException, InterruptedException {
        String curlCommand = "curl -o";
        String newFilename = "fileFromGitHub.java";
        Process proc = Runtime.getRuntime().exec(curlCommand + " " + newFilename + " " + rawLinkToFile);
        proc.waitFor(); 
        return newFilename;
    }
    public static void printAndAnalyse(String inputRawLinkToFile, String fileFromGitHub, String pathToXML) throws IOException, JWNLException, CloneNotSupportedException {
        System.out.println("| INPUTS:"
                            + "\n| Requirements: " + pathToXML
                            + "\n| GitHub file: " + inputRawLinkToFile
                            + "\n\n| ANALYSING..." + "\n");
        NameAnalyser.checkFiles(fileFromGitHub, pathToXML);
    }

}
