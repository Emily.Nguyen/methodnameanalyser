package com.github.emilymnl.methodnameanalyser.XML;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DomXMLParser {
    public static List<Map<String, Map<String, String>>> listCases = new ArrayList<Map<String, Map<String, String>>>();

    public static List<Map<String, Map<String, String>>> getXMLData(String filename) {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {
            dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);

            // parse XML file
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(new File(filename));
            doc.getDocumentElement().normalize();

            // get list of rules/cases
            NodeList caseList = doc.getDocumentElement().getChildNodes(); 

            // loop through rule/case
            for (int c = 0; c < caseList.getLength(); c++) {
                Node node = caseList.item(c);

                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;

                    if (element.getNodeName().equals("declare")) {
                        String ruleName = element.getAttributes().getNamedItem("rule").getNodeName();
                        String ruleForName = element.getAttributes().getNamedItem("for").getNodeName();
                        String ruleOpName = element.getAttributes().getNamedItem("op").getNodeName();
                        String ruleSizeName = element.getAttributes().getNamedItem("size").getNodeName();

                        String rule = element.getAttributes().getNamedItem(ruleName).getTextContent();
                        String ruleFor = element.getAttributes().getNamedItem(ruleForName).getTextContent();
                        String ruleOp = element.getAttributes().getNamedItem(ruleOpName).getTextContent();
                        String ruleSize = element.getAttributes().getNamedItem(ruleSizeName).getTextContent();
                        String caseAndRegex = ruleName + " " + rule;
                        
                        Map<String, Map<String, String>> caseMap = new HashMap<String, Map<String, String>>();
                        caseMap.put(caseAndRegex, new HashMap<String, String>());

                        caseMap.get(caseAndRegex).put(ruleForName, ruleFor);
                        caseMap.get(caseAndRegex).put(ruleOpName, ruleOp);
                        caseMap.get(caseAndRegex).put(ruleSizeName, ruleSize);
                        
                        // list of all nodes per filter
                        NodeList nodeList = element.getChildNodes();

                        // nodes/filters in rule  
                        String filterName = "";   
                        String filterValues = "";   
                        for (int filter = 0; filter < nodeList.getLength(); filter++) {     
                            Node filterNode = nodeList.item(filter);

                            if (filterNode.getNodeType() == Node.ELEMENT_NODE) {
                                filterName = nodeList.item(filter).getNodeName();
                                String filterValue = filterNode.getAttributes().getNamedItem("instance").getTextContent();
                                filterValues += filterValue + " ";
                            }  
                        }
                        caseMap.get(caseAndRegex).put(filterName, filterValues);
                        listCases.add(caseMap);
                    }
                    else if (element.getNodeName().equals("case")) {
                        // for cases
                        String method = element.getAttributes().getNamedItem("method").getNodeName();
                        String methodRegex = element.getAttributes().getNamedItem(method).getTextContent();
                        String caseAndRegex = method + " " + methodRegex;

                        Map<String, Map<String, String>> caseMap = new HashMap<String, Map<String, String>>();
                        caseMap.put(caseAndRegex, new HashMap<String, String>());

                        // list of all nodes per case
                        NodeList nodeList = element.getChildNodes(); 

                        List<String> uniqueFrequencyList = new ArrayList<String>();

                        // nodes/frequences in case       
                        for (int freq = 0; freq < nodeList.getLength(); freq++) {                       
                            if (nodeList.item(freq).getNodeType() == Node.ELEMENT_NODE) {
                                String frequency = nodeList.item(freq).getNodeName();

                                // no duplicate
                                if (!uniqueFrequencyList.contains(frequency)) { 
                                    uniqueFrequencyList.add(frequency);
                                    
                                    NodeList frequencyList = element.getElementsByTagName(frequency);
                                    String frequencyValues = "";

                                    // freq val in frequencylist
                                    for (int freqVal = 0; freqVal < frequencyList.getLength(); freqVal++) { 
                                        NodeList frequencyValueList = frequencyList.item(freqVal).getChildNodes();

                                        // loop thru all freq val in each frequencylist
                                        for (int value = 0; value < frequencyValueList.getLength(); value++) { 
                                            if (frequencyValueList.item(value).getNodeType() == Node.ELEMENT_NODE) {
                                                String frequencyValue = frequencyValueList.item(value).getNodeName();
                                                frequencyValues += frequencyValue + " ";
                                            }
                                        }
                                    }
                                    caseMap.get(caseAndRegex).put(frequency, frequencyValues);
                                }
                            }  
                        }
                        listCases.add(caseMap);
                    }
                }
            }
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
        return listCases;
    }
}
