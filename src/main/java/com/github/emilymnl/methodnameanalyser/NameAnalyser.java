package com.github.emilymnl.methodnameanalyser;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import com.github.emilymnl.methodnameanalyser.NLP.POS;
import com.github.emilymnl.methodnameanalyser.XML.DomXMLParser;

import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.PackageDeclaration;
import com.github.javaparser.ast.body.*;
import com.github.javaparser.ast.expr.*;

import net.sf.extjwnl.JWNLException;

public class NameAnalyser {
    public static void checkFiles(String classFile, String XMLFile) throws IOException, JWNLException, CloneNotSupportedException {
        List<Map<String, Map<String, String>>> XMLList = DomXMLParser.getXMLData(XMLFile);
        Map<String, Map<String, List<String>>> allResult = new HashMap <String, Map<String, List<String>>>();

        CompilationUnit compilationUnit = StaticJavaParser.parse(new FileReader(classFile));        

        // PACKAGE
        PackageDeclaration packageType = compilationUnit.getPackageDeclaration().get();
        String packageName = compilationUnit.getPackageDeclaration().get().getNameAsString();
        checkAllNamesToXML(packageName, "package", packageType, XMLList, allResult);

        // CLASS/INTERFACE
        List<TypeDeclaration<?>> classesOrInterfaces = compilationUnit.getTypes();
        for (TypeDeclaration<?> classOrInterface : classesOrInterfaces) {
            String classNameOrInterfaceName = classOrInterface.getNameAsString();
            checkAllNamesToXML(classNameOrInterfaceName, "classOrInterface", classOrInterface, XMLList, allResult);
            Optional<ClassOrInterfaceDeclaration> classOrInterfaceType = compilationUnit.getClassByName(classNameOrInterfaceName);

            // if not empty then there is a class
            if (!classOrInterfaceType.isEmpty()) {
                ClassOrInterfaceDeclaration classType = classOrInterfaceType.get();

                // FIELD (VARIABLE)
                List<FieldDeclaration> fieldList = classType.getFields();
                for (FieldDeclaration field : fieldList) {
                    String fieldName = field.toString();
                    checkAllNamesToXML(fieldName, "variable", field, XMLList, allResult);
                }

                // METHOD
                List<MethodDeclaration> methodList = classType.getMethods();
                for (MethodDeclaration method : methodList) {
                    String methodName = method.getNameAsString();
                    checkAllNamesToXML(methodName, "method", method, XMLList, allResult);

                    // VARIABLE (VARIABLE)
                    List<Node> variableList = method.stream()
                                            .filter(m -> m instanceof VariableDeclarator)
                                            .collect(Collectors.toList());
                    for (Node variable : variableList) {
                        String variableName = variable.getParentNode().get().toString();
                        checkAllNamesToXML(variableName, "variable", variable, XMLList, allResult);
                    }
                }
            } 
        }
        printResult(allResult);
    }

    // combine old and new maps of same identifier type, and handle null and duplicates
    public static void checkAllNamesToXML(String nodeName, String nodeType, Node nodeContent, List<Map<String, Map<String, String>>> XMLList, Map<String, Map<String, List<String>>> allResult) throws IOException, JWNLException, CloneNotSupportedException {
        checkToXML(nodeName, nodeType, nodeContent, XMLList).forEach(
            (key, value) -> allResult.merge( key, value, (v1, v2) -> { 
                if (v1.isEmpty()) {
                    return v2;
                } else if (!(v1.equals(v2)) && !v2.isEmpty()) {
                    v1.putAll(v2);
                    return v1;
                } else {
                    return v1;
                }
            })
        );
    }

    public static Map<String, Map<String, List<String>>> checkToXML(String nodeName, String nodeType, Node nodeContent, List<Map<String, Map<String, String>>> XMLList) throws IOException, JWNLException, CloneNotSupportedException {
        List<Map<String, Map<String, String>>> rules = new ArrayList<Map<String, Map<String, String>>>();
        List<Map<String, Map<String, String>>> identifiers = new ArrayList<Map<String, Map<String, String>>>();
        Map<String, Map<String, List<String>>> mapOfResult = new HashMap <String, Map<String, List<String>>>();
        
        // split the rules and identifier cases
        for (Map<String, Map<String, String>> XMLModel : XMLList) {
            for (String caseKey : XMLModel.keySet()) {
                String[] caseKeyArray = caseKey.split("\\s+", 2);
                String caseType = caseKeyArray[0];
                if (caseType.equals("rule")) {
                    rules.add(XMLModel);
                } else {
                    identifiers.add(XMLModel);
                }
            }
        }

        // each case from identifiers
        for (Map<String, Map<String, String>> identifierMap : identifiers) {
            // loop through each case and get type and condition from xml
            for (String caseKey : identifierMap.keySet()) {
                String[] caseKeyArray = caseKey.split("\\s+", 2);
                String caseIdentifier = caseKeyArray[0];
                String caseCondition = caseKeyArray[1];

                // add the caseCondition to map
                mapOfResult.put(caseCondition, new HashMap<String, List<String>>());

                if (caseIdentifier.equals(nodeType)) {
                    if (POS.checkRegexToName(caseCondition, nodeName)) {
                        List<String> checkedRequirementList = new ArrayList<String>();
                        for (String subCaseKey : identifierMap.get(caseKey).keySet()) {
                            String requirements = identifierMap.get(caseKey).get(subCaseKey);
                            List<String> requirementList = new ArrayList<String>(Arrays.asList(requirements.split(" ")));

                            if (caseIdentifier.equals("package")) {
                                for (String requirement : requirementList) {
                                    System.out.println("Package '" + nodeName + "' should be checked for the requirement: " + requirement);
                                }
                            } else if (caseIdentifier.equals("classOrInterface")) {
                                for (String requirement : requirementList) {
                                    System.out.println("Class/Interface '" + nodeName + "' should be checked for the requirement: " + requirement);
                                }
                            } else if (caseIdentifier.equals("variable")) {
                                for (String requirement : requirementList) {
                                    System.out.println("Variable '" + nodeName + "' should be checked for the requirement: " + requirement);
                                }
                            } else if (caseIdentifier.equals("method")) {
                                for (String requirement : requirementList) { 
                                    // if pre-defined requirement
                                    if (requirement.equals("callsMethodWithSameName")) {
                                        String resultSameName = getPassOrFail(subCaseKey, callsMethodWithSameName((MethodDeclaration)nodeContent));
                                        if (!resultSameName.isEmpty()) {
                                            String requirementResult = resultSameName + ": " + requirement + " [" + subCaseKey + "]";
                                            checkedRequirementList.add(requirementResult); 
                                        } 
                                    // customised requirement
                                    } else {
                                        for (Map<String, Map<String, String>> ruleMap : rules) {
                                            for (String ruleKey : ruleMap.keySet()) {
                                                String[] ruleKeyArray = ruleKey.split("\\s+", 2);
                                                String ruleName = ruleKeyArray[1];

                                                if (requirement.equals(ruleName)) {
                                                    String ruleOp = ruleMap.get(ruleKey).get("op");
                                                    int ruleSize = Integer.parseInt(ruleMap.get(ruleKey).get("size"));
                                                    String ruleFilter = ruleMap.get(ruleKey).get("filter");
                                                    String[] filterList = ruleFilter.split(" ");
                                                    
                                                    String resultRest = getPassOrFail(subCaseKey, ruleChecker(nodeContent, filterList, ruleOp, ruleSize));
                                                    if (!resultRest.isEmpty()) {
                                                        String requirementResult = resultRest + ": " + requirement + " [" + subCaseKey + "]";
                                                        checkedRequirementList.add(requirementResult);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    // add the identifier + lists of requirements
                                    mapOfResult.get(caseCondition).put("'" + nodeName + "' (" + caseIdentifier + ")", checkedRequirementList);
                                }
                            }
                        }
                    }
                }
            }
        }
        return mapOfResult;
    }

    public static String getPassOrFail(String frequency, boolean hasRequirement) {
        String result = "";
        if ((hasRequirement && (!frequency.equals("never"))) || ((!hasRequirement) && frequency.equals("never"))) {
            result = "PASS";
        } else if ((hasRequirement && frequency.equals("never")) || ((!hasRequirement) && (!frequency.equals("never")))){
            result = "FAIL";
        }
        return result;
    }

    public static String printResult(Map<String, Map<String, List<String>>> mapOfResult) {
        for (String eachCase : mapOfResult.keySet()) {
            System.out.println("--------------------------------------\n|case " + eachCase + " matches:\n|");

            if (!mapOfResult.get(eachCase).keySet().isEmpty()) {
                for (String identifier : mapOfResult.get(eachCase).keySet()) {
                    System.out.println("|    " + identifier + ":"); 

                    for (Object requirement : mapOfResult.get(eachCase).get(identifier)) {
                        System.out.println("|        " + requirement); 
                    }
                    System.out.println("|");
                }
            } else {
                System.out.println("|    (none)\n|");
            }
        }
        System.out.println("--------------------------------------");
        return "";
    }

    public static boolean callsMethodWithSameName(MethodDeclaration method) throws IOException, JWNLException, CloneNotSupportedException {
        String methodName = method.getNameAsString(); 
        List<Node> methodCallList = method.stream()
                                .filter(m -> m instanceof MethodCallExpr)
                                .collect(Collectors.toList());
        // loop through method calls, get only the names
        for(Node methodCallBody : methodCallList) {
            String methodCallName = ((MethodCallExpr) methodCallBody).getNameAsString();
            // find pos to method name
            String methodNamePOS = String.join("", POS.getPOS(methodName, true));
            // check method name and method call name
            if (POS.checkRegexToName(methodNamePOS, methodCallName)) {
                return true;
            }
        }
        return false;
    }

    public static boolean ruleChecker(Node caseTypeInput, String[] listOfInstances, String comparisonOperator, int comparisonValue) {
        List<Object> listOfFoundInstances = new ArrayList<Object>();
        for (String instanceString : listOfInstances) {
            List<Node> foundInstances = caseTypeInput.stream().filter(m -> {
                try {
                    return Class.forName(instanceString).isInstance(m);
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                    return false;
                } 
            } )
            .collect(Collectors.toList());
            
            if (foundInstances.size() > 0) {
                listOfFoundInstances.add(foundInstances);
            }
        }
        
        boolean ruleResult = false;
        if (comparisonOperator.equals("LT")) {
            ruleResult = listOfFoundInstances.size() < comparisonValue;
        } else if (comparisonOperator.equals("GT")) {
            ruleResult = listOfFoundInstances.size() > comparisonValue;
        }  else if (comparisonOperator.equals("EQ")) {
            ruleResult = listOfFoundInstances.size() == comparisonValue;
        } else if (comparisonOperator.equals("NE")) {
            ruleResult = listOfFoundInstances.size() != comparisonValue;
        } else if (comparisonOperator.equals("GE")) {
            ruleResult = listOfFoundInstances.size() >= comparisonValue;
        } else if (comparisonOperator.equals("LE")) {
            ruleResult = listOfFoundInstances.size() <= comparisonValue;
        } 
    return ruleResult;
	}
}